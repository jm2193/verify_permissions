#!/usr/bin/env python3
import argparse
import datetime
import subprocess
import json
import os

def check_perms(file, perms, directory=False):
    '''
    Checks permissions on a file and returns missing and incorrect
    permissions
    '''
    if not directory:
        perms = [perm for perm in perms if 'default' not in perm]
    try:
        raw_perms = subprocess.check_output(['getfacl', '-p', \
                                             file], \
                                             universal_newlines=True)
    except subprocess.CalledProcessError:
        return (False, False)
    fmt_perms = [entry for entry in raw_perms.strip('\n').split('\n') \
                 if '#' not in entry]
    incorrect_perms = [perm for perm in fmt_perms if perm not in perms \
                       and '::' not in perm \
                       and not perm.startswith('user')]
    missing_perms = [perm for perm in perms if perm not in fmt_perms \
                       and '::' not in perm \
                       and not perm.startswith('user')]
    # Now that we are done comparing, we can return them as simple strings
    if incorrect_perms:
        inc_return = '\n'.join(incorrect_perms)
    else:
        inc_return = False
    if missing_perms:
        mis_return = '\n'.join(missing_perms)
    else:
        mis_return = False
    return (inc_return, mis_return, raw_perms)

def check_data(group, path, acls):
    '''
    Assumes that path is /mnt/data/group
    '''
    try:
        perm_root = os.path.join(path, '.permissions')
        os.mkdir(perm_root)
    except FileExistsError:
        pass
    incorrect = {}
    missing = {}
    with open(os.path.join(perm_root, 'original.txt'), 'w') as orig:
        for root, directory, files in os.walk(path):
            inc_fol, mis_fol, raw_fol = check_perms(root, acls, \
                                                    directory = True)
            if inc_fol:
                if inc_fol in incorrect:
                    incorrect[inc_fol].append({'root': root})
                else:
                    incorrect[inc_fol] = [{'root' : root}]
            if mis_fol:
                if mis_fol in missing:
                    missing[mis_fol].append({'root': root})
                else:
                    missing[mis_fol] = [{'root': root}]
            orig.write(raw_fol)
            for this_file in files:
                this_full_path = os.path.join(root, this_file)
                this_dict = {'root': root, 'file': this_file}
                inc_file, mis_file, raw_file = check_perms(this_full_path, acls)
                if inc_file:
                    if inc_file in incorrect:
                        incorrect[inc_file].append(this_dict)
                    else:
                        incorrect[inc_file] = [this_dict]
                if mis_file:
                    if mis_file in missing:
                        missing[mis_file].append(this_dict)
                    else:
                        missing[mis_file] = [this_dict]
                orig.write(raw_file)
    with open(os.path.join(perm_root, 'report.txt') \
              'w') as report_file:
        report = {'incorrect': incorrect, \
                  'missing': missing}
        report_file.write(json.dumps(report, indent=4))

def remove_acl(file, acl):
    '''
    Removes an acl set from a file
    '''
    # For printing purposes, acls are newline separated, sort that first
    fmt_acl = ""
    for perm in acl.split('\n'):
        this_perm = perm.split(':')
        # Drop the last bit
        this_fmt = this_perm[0] + ':' + this_perm[1]
        if fmt_acl:
            fmt_acl = fmt_acl + ',' + this_fmt
        else:
            fmt_acl = this_fmt
    print(fmt_acl)
    try:
        remove_perm = subprocess.check_output(['setfacl', '-x', \
                                               fmt_acl, file])
    except subprocess.CalledProcessError:
        return False
    return True

def apply_acl(file, acl):
    '''
    Applies an acl to a file
    '''
    fmt_acl = acl.replace('\n', ',')
    print(f'Applying {acl} to {file}')
    try:
        apply_perm = subprocess.check_output(['setfacl', '-m', \
                                              fmt_acl, file])
    except subprocess.CalledProcessError:
        return False
    return True

def query_yes_no(question, default='no'):
    '''
    What do you think it does
    '''
    valid = {'yes': True, 'y': True, 'ye': True, \
             'no': False, 'n': False}
    while True:
        choice = input(question).lower()
        if choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            print('Please respond y/n')

def print_permissions(perm, files):
    '''
    Prettily (eye of the beholder) prints out a set of permissions and how many
    files/folders are affected
    '''
    print('Permissions set:\n')
    print(perm)
    files_affected = [path for path in files if not path.get('file', False)]
    dirs_affected = [path for path in files if path.get('file', False)]
    print(f'Files affected: {len(files_affected)}')
    print(f'Folders affected: {len(dirs_affected)}')

def verify_permissions(group, path, acls):
    '''
    Either reads a report, or generates one and then modifies permissions but
    only with consent
    '''
    if acls is not None:
        perms = acls
    else:
        perms = ['group:sec_nas-server_all_data_rw:rwx',
                            f'group:sec_tier2_{group}_data_rw:rwx',
                            'default:user:rwx',
                            'default:group:rwx',
                            'default:group:sec_nas-server_all_data_rw:rwx',
                            f'default:group:sec_tier2_{group}_data_rw:rwx',
                            'default:mask:rwx',
                            'default:other::---']
    if path is not None:
        root_path = path
    else:
        root_path = (f'/mnt/data/{group}/group_folders')
    reportfile = os.path.join(root_path, '.permissions', 'report.txt')
    if not os.path.exists(reportfile):
        print('No report, running now. Don\'t hold your breath.\n')
        check_data(group, root_path, perms)
    else:
        print(f'Report exists, created on: ' \
              f'{datetime.datetime.fromtimestamp(os.stat(reportfile).st_mtime)}')
        if not query_yes_no('Use this report to modify permissions? [y/N]: '):
            if not query_yes_no('Generate new report (this will take a while)' \
                                '[y/N]: '):
                return
            else:
                check_data(group, root_path, perms)
    print('Loading report...\n')
    with open(reportfile, 'r') as report:
        full_report = json.loads(report.read())
    print('Incorrect permissions:\n')
    for perm in full_report['incorrect']:
        print_permissions(perm, full_report['incorrect'][perm])
        if query_yes_no('Remove these permissions? [y/N]: '):
            for path in [os.path.join(entry['root'], entry.get('file', '')) \
                         for entry in full_report['incorrect'][perm]]:
                remove_acl(path, perm)
    print('\n\nMissing permissions:\n')
    for perm in full_report['missing']:
        print_permissions(perm, full_report['missing'][perm])
        if query_yes_no('Apply these permissions? [y/N]: '):
            for path in [os.path.join(entry['root'], entry.get('file', '')) \
                         for entry in full_report['missing'][perm]]:
                apply_acl(path, perm)

def main():
    '''
    Parse options and do stuff
    '''
    path = None
    acls = None
    subfolder = None
    parser = argparse.ArgumentParser()
    parser.add_argument('group', nargs=1, \
        help='Group to verify (eg. computing)')
    parser.add_argument('-p', '--path', nargs='?', \
        help='Full path if different from /mnt/data/{group}')
    parser.add_argument('-s', '--subfolder', nargs='?', \
        help='Subfolder of path to verify, defaults to group_folders')
    parser.add_argument('-a', '--acls', nargs='?', \
        help='Newline (\\n) separated ACL defaults')
    args = parser.parse_args()
    if args.path:
        path = args.path[0]
    if args.acls:
        acls = args.acls[0]
    if args.subfolder:
        subfolder = args.subfolder[0]
    if not args.group:
        print('Need to specify group to verify')
        os._exit()
    verify_permissions(args.group[0], path, acls, subfolder)

if __name__ == '__main__':
    main()
