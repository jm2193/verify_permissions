#!/bin/bash

SRV_ALL_DATA="sec_`hostname -s`_all_data_rw"
SRV_ALL_GROUP_R="sec_`hostname -s`_all_group_r"
SRV_ALL_GROUP_RW="sec_`hostname -s`_all_group_rw"
# This variable is in place as a temporary measure but could potentially be
# useful in the long run - at the moment all sec groups have nas-server in them
# but we plan to change this to tier2 at some point
SEC_CHANGE="nas-server"
# Core list not expected to change much but worth separating out
CORE_LIST="bioinformatics biological_resources biorepository flow_cytometry genome_editing genomics histopathology imaging light_microscopy pharmacokinetics proteomics research_instrumentation scientific_administration"

verify_shared_folders () {
    local MOUNTPOINT=$1
    local GROUP=$2
    local SEC_DATA_RW="sec_"$SEC_CHANGE"_"$GROUP"_data_rw"

    # Loop through the list of cores and create their respective shared folders
    # with the correct permissions
    for CORE in $CORE_LIST;
    do
        mkdir -v $MOUNTPOINT/shared_folders/$CORE
        setfacl -R -m d:g:"sec_"$SEC_CHANGE"_"$CORE"_data_rw":rwx,g:"sec_"$SEC_CHANGE"_"$CORE"_data_rw":rwx \
        $MOUNTPOINT/shared_folders/$CORE
    done

    # Apply generic permissions for the group to all folders just created
    setfacl -R -m d:g:$SRV_ALL_DATA:rwx,g:$SRV_ALL_DATA:rwx $MOUNTPOINT/shared_folders/*
    setfacl -R -m d:g:$SEC_DATA_RW:rwx,g:$SEC_DATA_RW:rwx $MOUNTPOINT/shared_folders/*

    # Apply default other and group
    setfacl -R -m d::g:rwx,g::rwx,d::o:---,o:--- $MOUNTPOINT/*
    setfacl -R -m d::o:rx,o:rx $MOUNTPOINT/shared_folders
}

check_ad_group () {
    local GROUP=$1

    if wbinfo -g | grep $GROUP
    then
        echo "Group name present in AD"
    else
        echo "Group name not found, please create in AD"
        exit 1
    fi
}

main () {
    read -p "Enter group name: " DSNAME

    check_ad_group $DSNAME

    MOUNTPOINT="/mnt/data/$DSNAME"
    verify_shared_folders($MOUNTPOINT, $DSNAME)
}

main
